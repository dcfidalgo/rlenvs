# DeepMind Lab

> DeepMind Lab is a 3D learning environment based on id Software's Quake III Arena via ioquake3 and other open source software.
> DeepMind Lab provides a suite of challenging 3D navigation and puzzle-solving tasks for learning agents. 
> Its primary purpose is to act as a testbed for research in artificial intelligence, especially deep reinforcement learning.

*https://github.com/google-deepmind/lab*

This container includes PyTorch since it is built on top of Nvidia's PyTorch container.
Feel free to switch out the base container in the definition file, or install other Python frameworks in the `%post` section.

## Build the container 

**Don't build containers on the [login nodes](https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html#resource-limits)! Either submit a SLURM job, use an RVS Jupyter Lab session, or use one of the interactive nodes.**

```bash
module load apptainer/1.3.2
apptainer build deepmind_lab.sif deepmind_lab.def
```

## Run the container

```bash
apptainer exec --nv --bind /usr/share/glvnd rele.sif python your_script.py
```

- the `--nv` flag is necessary to expose the Nvidia devices in the container and for CUDA to work;
- the `--bind /usr/share/glvnd` option is necessary for EGL to find the Nvidia GPU (for rendering);

